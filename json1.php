<?php

require_once './contoller/Servicio.php';
require_once './contoller/conexion.php';

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, Content-Type, api-token');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
header('Content-type: application/json');
header("HTTP/1.1 200");
$uri = $_SERVER['REQUEST_URI'];
$dir = explode("json1.php/", $uri);
if (count($dir) >= 2) {
    $tokenDir = explode("/", $dir[1]);
    //por lo general el primero es la accion
    $accion = $tokenDir[0];
    if ($accion == 'guardar_proyecto') {
        $token = obtener_header("api-token");
        registro_proyecto($token);
    }
    if ($accion == 'modificar_proyecto') {
        if (isset($tokenDir[1]) && $tokenDir[1] != "") {
            $token = obtener_header("api-token");
            modificar_proyecto($token, $tokenDir[1]);
        } else {
            echo json_encode(array("message" => "Recurso no encontrado"));
            http_response_code(404);
        }
    }
    
} else {
    echo json_encode(array("message" => "Recurso no encontrado"));
    http_response_code(404);
}

function registro_proyecto($token) {
    if ($token != "" && $token == 'EXAMEN_4_B') {
        $json = file_get_contents('php://input');
        $datos = json_decode($json);
        $wsdl = new \contoller\Servicio();
        if (strlen(trim($datos->nombre)) > 0 
                && strlen(trim($datos->finicio)) > 0
                && strlen(trim($datos->ffinalizacion)) > 0
                && strlen(trim($datos->tiempo)) > 0
                && strlen(trim($datos->provincia)) > 0
                && strlen(trim($datos->canton)) > 0
                && strlen(trim($datos->pais)) > 0
                && strlen(trim($datos->extranjero)) > 0
                && strlen(trim($datos->descripcion)) > 0
                && strlen(trim($datos->monto)) > 0
                && strlen(trim($datos->id_departamento)) > 0) {
            $data = array();
            $data['nombre'] = trim($datos->nombre);
            $data['fi'] = trim($datos->finicio);
            $data['ff'] = trim($datos->ffinalizacion);
            $data['time'] = trim($datos->tiempo);
            $data['prov'] = trim($datos->provincia);
            $data['cant'] =trim($datos->canton);
            $data['pais'] = trim($datos->pais);
            $data['extran'] = (trim($datos->extranjero) == 'si') ? true : false;
            $data['desc'] = trim($datos->descripcion);
            $data['monto'] = trim($datos->monto);
            $data['id'] = trim($datos->id_departamento);
            $save = $wsdl->guardar($token, $data);
            if (count($save) > 0) {
                echo json_encode($save);
            } else {
                echo json_encode(array("message" => "No se pudo guardar",
                "codigo" => "500"));
            }
        } else {
            echo json_encode(array("message" => "FALTAN DATOS",
                "codigo" => "400"));
        }
    } else {
        echo json_encode(array("mensaje" => "FALTA TOKEN",
            "codigo" => "401"));
    }
}

function modificar_proyecto($token, $external) {
    if ($token != "" && $token == 'EXAMEN_4_B') {
        $json = file_get_contents('php://input');
        $datos = json_decode($json);
        $wsdl = new \contoller\Servicio();
        if (strlen(trim($datos->nombre)) > 0 
                && strlen(trim($external)) > 0
                && strlen(trim($datos->finicio)) > 0
                && strlen(trim($datos->ffinalizacion)) > 0
                && strlen(trim($datos->tiempo)) > 0
                && strlen(trim($datos->provincia)) > 0
                && strlen(trim($datos->canton)) > 0
                && strlen(trim($datos->pais)) > 0
                && strlen(trim($datos->extranjero)) > 0
                && strlen(trim($datos->descripcion)) > 0
                && strlen(trim($datos->monto)) > 0
                && strlen(trim($datos->id_departamento)) > 0) {
            $data = array();
            $data['external'] = $external;
            $data['nombre'] = trim($datos->nombre);
            $data['fi'] = trim($datos->finicio);
            $data['ff'] = trim($datos->ffinalizacion);
            $data['time'] = trim($datos->tiempo);
            $data['prov'] = trim($datos->provincia);
            $data['cant'] =trim($datos->canton);
            $data['pais'] = trim($datos->pais);
            $data['extran'] = (trim($datos->extranjero) == 'si') ? true : false;
            $data['desc'] = trim($datos->descripcion);
            $data['monto'] = trim($datos->monto);
            $data['id'] = trim($datos->id_departamento);
            $save = $wsdl->modificar($token, $data);
            if (count($save) > 0) {
                echo json_encode($save);
            } else {
                echo json_encode(array("message" => "No se pudo guardar",
                "codigo" => "500"));
            }
        } else {
            echo json_encode(array("message" => "FALTAN DATOS",
                "codigo" => "400"));
        }
    } else {
        echo json_encode(array("mensaje" => "FALTA TOKEN",
            "codigo" => "401"));
    }
}

function obtener_header($nombreToken) {
    $valorToken = "";
    foreach (getallheaders() as $nombre => $valor) {

        if ($nombre == $nombreToken)
            $valorToken = $valor;
    }
    $headers = apache_request_headers();
    foreach ($headers as $nombre => $valor) {
        if ($nombre == $nombreToken)
            $valorToken = $valor;
//        echo "$nombre: $valor\n";
    }
    return $valorToken;
}
