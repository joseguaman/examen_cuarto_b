<?php

require_once './contoller/Servicio.php';
require_once './contoller/conexion.php';
require_once './nusoap/lib/nusoap.php';

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header('Content-type: application/xml');

$server = new \soap_server();
$server->configureWSDL('server', 'urn:server');
$server->wsdl->schemaTargetNamespace = 'urn:server';

$server->wsdl->addComplexType(
        'mensaje',
        'complexType',
        'struct',
        'all',
        '',
        array(
            'mensaje' => array('name' => 'mensaje',
                'type' => 'xsd:string'), 
            'codigo' => array('name' => 'codigo',
                'type' => 'xsd:string'))
);


//------------------ REGISTROS ---------//


$server->register('guardar_proyecto',
        array("token" => "xsd:string",
            "nombre" => "xsd:string",
            "finicio" => "xsd:string",
            "ffinalizacion" => "xsd:string",
            "tiempo" => "xsd:integer",
            "provincia" => "xsd:string",
            "canton" => "xsd:string",
            "pais" => "xsd:string",
            "extranjero" => "xsd:string",
            "descripcion" => "xsd:string",
            "monto" => "xsd:decimal",
            "id" => "xsd:integer"), // parameter
        array('return' => 'tns:mensaje'), // output  array('return' => 'xsd:string'),     // output
        'urn:server', // namespace
        'urn:server#helloServer', // soapaction
        'rpc', // style
        'encoded', // use
        'Guardar proyecto');                   // description

$server->register('modificar_proyecto',
        array("token" => "xsd:string",
            "external" => "xsd:string",
            "nombre" => "xsd:string",
            "finicio" => "xsd:string",
            "ffinalizacion" => "xsd:string",
            "tiempo" => "xsd:integer",
            "provincia" => "xsd:string",
            "canton" => "xsd:string",
            "pais" => "xsd:string",
            "extranjero" => "xsd:string",
            "descripcion" => "xsd:string",
            "monto" => "xsd:decimal",
            "id_departamento" => "xsd:integer"), // parameter
        array('return' => 'tns:mensaje'), // output  array('return' => 'xsd:string'),     // output
        'urn:server', // namespace
        'urn:server#obtener_departamento', // soapaction
        'rpc', // style
        'encoded', // use
        'Permite modificar un proyecto');                   // description
//----------------- FIN DE REGUSTROS -------//

function guardar_proyecto($token, $nombre, $finicio, $ffinalizacion, $tiempo, $provincia, 
        $canton, $pais, $extranjero, $descripcion, $monto, $id_departamento) {
    $wsdl = new \contoller\Servicio();
    if($token && $token == 'EXAMEN_4_B') {
        if (strlen(trim($nombre)) > 0 && strlen(trim($finicio)) > 0
                && strlen(trim($ffinalizacion)) > 0
                && strlen(trim($tiempo)) > 0
                && strlen(trim($provincia)) > 0
                && strlen(trim($canton)) > 0
                && strlen(trim($pais)) > 0
                && strlen(trim($extranjero)) > 0
                && strlen(trim($descripcion)) > 0
                && strlen(trim($monto)) > 0
                && strlen(trim($id_departamento)) > 0) {
            $data = array();
            $data['nombre'] = $nombre;
            $data['fi'] = $finicio;
            $data['ff'] = $ffinalizacion;
            $data['time'] = $tiempo;
            $data['prov'] = $provincia;
            $data['cant'] =$canton;
            $data['pais'] = $pais;
            $data['extran'] = ($extranjero == 'si') ? true : false;
            $data['desc'] = $descripcion;
            $data['monto'] = $monto;
            $data['id'] = $id_departamento;
            $save = $wsdl->guardar($token, $data);
            if (count($save) > 0) {
                return $save;
            } else {
                return new soap_fault('500', '', "No se pudo guardar", '');
            }
        } else {
            return new soap_fault('500', '', 'Faltan datos', '');
        }
        $datos = $wsdl->obtener_departamentos();
        return $datos;
    } else {
        return new soap_fault('500', '', 'Falta token', '');
    }
}

function modificar_proyecto($token, $external, $nombre, $finicio, $ffinalizacion, $tiempo, $provincia, 
        $canton, $pais, $extranjero, $descripcion, $monto, $id_departamento) {
    $wsdl = new \contoller\Servicio();
    if($token && $token == 'EXAMEN_4_B') {
        if (strlen(trim($nombre)) > 0 && strlen(trim($finicio)) > 0
                && strlen(trim($ffinalizacion)) > 0
                && strlen(trim($external)) > 0
                && strlen(trim($tiempo)) > 0
                && strlen(trim($provincia)) > 0
                && strlen(trim($canton)) > 0
                && strlen(trim($pais)) > 0
                && strlen(trim($extranjero)) > 0
                && strlen(trim($descripcion)) > 0
                && strlen(trim($monto)) > 0
                && strlen(trim($id_departamento)) > 0) {
            $data = array();
            $data['nombre'] = $nombre;
            $data['external'] = $external;
            $data['fi'] = $finicio;
            $data['ff'] = $ffinalizacion;
            $data['time'] = $tiempo;
            $data['prov'] = $provincia;
            $data['cant'] =$canton;
            $data['pais'] = $pais;
            $data['extran'] = ($extranjero == 'si') ? true : false;
            $data['desc'] = $descripcion;
            $data['monto'] = $monto;
            $data['id'] = $id_departamento;
            $save = $wsdl->modificar($token, $data);
            if (count($save) > 0) {
                return $save;
            } else {
                return new soap_fault('500', '', "No se pudo modificar", '');
            }
        } else {
            return new soap_fault('500', '', 'Faltan datos', '');
        }
        $datos = $wsdl->obtener_departamentos();
        return $datos;
    } else {
        return new soap_fault('500', '', 'Falta token', '');
    }
}
$server->service(file_get_contents("php://input"));