<?php

require_once './contoller/Servicio.php';
require_once './contoller/conexion.php';

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, Content-Type, api-token');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");
header('Content-type: application/json');
header("HTTP/1.1 200");
$uri = $_SERVER['REQUEST_URI'];
$dir = explode("json2.php/", $uri);
if (count($dir) >= 2) {
    $tokenDir = explode("/", $dir[1]);
    //por lo general el primero es la accion
    $accion = $tokenDir[0];
    if ($accion == 'listar_proyectos') {
        $token = obtener_header("api-token");
        listar_proyectos($token);
    }
    if ($accion == 'listar_departamentos') {
        $token = obtener_header("api-token");
        listar_departamentos($token);
    }
    if ($accion == 'listar_provincias') {        
        listar_provincias();
    }
    if ($accion == 'listar_cantones') {        
        if (isset($tokenDir[1]) && $tokenDir[1] != "") {
           // $token = obtener_header("api-token");        
            $aux = explode("_", $tokenDir[1]);
            $aux1 = "";
            foreach ($aux as $item) {
                $aux1 .= $item . " ";
            }            
            $aux1 = trim($aux1);            
            listar_cantones($aux1);
        } else {
            echo json_encode(array("message" => "Recurso no encontrado"));
            http_response_code(404);
        }
    }
    
    if ($accion == 'obtener_departamento') {        
        if (isset($tokenDir[1]) && $tokenDir[1] != "") {
            $token = obtener_header("api-token");                         
            obtener_departamento($token, $tokenDir[1]);
        } else {
            echo json_encode(array("message" => "Recurso no encontrado"));
            http_response_code(404);
        }
    }
    
} else {
    echo json_encode(array("message" => "Recurso no encontrado"));
    http_response_code(404);
}

function listar_proyectos($token) {
    if ($token != "" && $token == 'EXAMEN_4_B') {
        $wsdl = new \contoller\Servicio();
        echo json_encode($wsdl->obtener_proyectos());
    } else {
        echo json_encode(array("mensaje" => "FALTA TOKEN",
            "codigo" => "401"));
    }
}

function listar_departamentos($token) {
    if ($token != "" && $token == 'EXAMEN_4_B') {
        $wsdl = new \contoller\Servicio();
        echo json_encode($wsdl->obtener_departamentos());
    } else {
        echo json_encode(array("mensaje" => "FALTA TOKEN",
            "codigo" => "401"));
    }
}

function listar_provincias() {
    $wsdl = new \contoller\Servicio();
        echo json_encode($wsdl->obtener_provincias_all());
}

function listar_cantones($provincia) {
    $wsdl = new \contoller\Servicio();
        echo json_encode($wsdl->obtener_cantones($provincia));
}

function obtener_departamento($token, $id) {
    if ($token != "" && $token == 'EXAMEN_4_B') {
        $wsdl = new \contoller\Servicio();
        echo json_encode($wsdl->getDepartamento($id));
    } else {
        echo json_encode(array("mensaje" => "FALTA TOKEN",
            "codigo" => "401"));
    }    
}



function obtener_header($nombreToken) {
    $valorToken = "";
    foreach (getallheaders() as $nombre => $valor) {

        if ($nombre == $nombreToken)
            $valorToken = $valor;
    }
    $headers = apache_request_headers();
    foreach ($headers as $nombre => $valor) {
        if ($nombre == $nombreToken)
            $valorToken = $valor;
//        echo "$nombre: $valor\n";
    }
    return $valorToken;
}
