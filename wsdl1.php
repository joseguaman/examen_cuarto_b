<?php

require_once './contoller/Servicio.php';
require_once './contoller/conexion.php';
require_once './nusoap/lib/nusoap.php';

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');
header('Content-type: application/xml');

$server = new \soap_server();
$server->configureWSDL('server', 'urn:server');
$server->wsdl->schemaTargetNamespace = 'urn:server';

$server->wsdl->addComplexType(
        'departamento',
        'complexType',
        'struct',
        'all',
        '',
        array('id' => array('name' => 'id',
                'type' => 'xsd:int'),
            'nombre' => array('name' => 'nombre',
                'type' => 'xsd:string'), 
            'tipo' => array('name' => 'tipo',
                'type' => 'xsd:string'))
);

$server->wsdl->addComplexType(
        'proyecto',
        'complexType',
        'struct',
        'all',
        '',
        array('external' => array('name' => 'external',
                'type' => 'xsd:string'),
            'nombre' => array('name' => 'nombre',
                'type' => 'xsd:string'),
            'f_inicio' => array('name' => 'f_inicio',
                'type' => 'xsd:string'),
            'f_finalizacion' => array('name' => 'f_finalizacion',
                'type' => 'xsd:string'),
            'provincia' => array('name' => 'provincia',
                'type' => 'xsd:string'),
            'canton' => array('name' => 'canton',
                'type' => 'xsd:string'),
            'pais' => array('name' => 'pais',
                'type' => 'xsd:string'),
            'extranjero' => array('name' => 'extranjero',
                'type' => 'xsd:string'),
            'descripcion' => array('name' => 'descripcion',
                'type' => 'xsd:string'),
            'monto' => array('name' => 'monto',
                'type' => 'xsd:string'),
            'id_departamento' => array('name' => 'id_departamento',
                'type' => 'xsd:string')
            )
);

$server->wsdl->addComplexType(
        'provincia',
        'complexType',
        'struct',
        'all',
        '',
        array('nombre' => array('name' => 'nombre',
                'type' => 'xsd:string'),
            'region' => array('name' => 'region',
                'type' => 'xsd:string'))
);

$server->wsdl->addComplexType(
        'canton',
        'complexType',
        'struct',
        'all',
        '',
        array('canton' => array('name' => 'canton',
                'type' => 'xsd:string'),
            'provincia' => array('name' => 'provincia',
                'type' => 'xsd:string'))
);

$server->wsdl->addComplexType('lista_departamento', 'complexType', 'array', '',
        'SOAP-ENC:Array', array(),
        array(array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:departamento[]')),
        'tns:departamento');

$server->wsdl->addComplexType('lista_proyecto', 'complexType', 'array', '',
        'SOAP-ENC:Array', array(),
        array(array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:proyecto[]')),
        'tns:proyecto');

$server->wsdl->addComplexType('lista_provincia', 'complexType', 'array', '',
        'SOAP-ENC:Array', array(),
        array(array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:provincia[]')),
        'tns:provincia');

$server->wsdl->addComplexType('lista_canton', 'complexType', 'array', '',
        'SOAP-ENC:Array', array(),
        array(array('ref' => 'SOAP-ENC:arrayType', 'wsdl:arrayType' => 'tns:canton[]')),
        'tns:canton');

//------------------ REGISTROS ---------//
$server->register('lista_departamento',
        array("token" => "xsd:string"), // parameter
        array('return' => 'tns:lista_departamento'), // output  array('return' => 'xsd:string'),     // output
        'urn:server', // namespace
        'urn:server#helloServer', // soapaction
        'rpc', // style
        'encoded', // use
        'Permite listar todos los departamentos');                   // description

$server->register('lista_proyecto',
        array("token" => "xsd:string"), // parameter
        array('return' => 'tns:lista_proyecto'), // output  array('return' => 'xsd:string'),     // output
        'urn:server', // namespace
        'urn:server#helloServer', // soapaction
        'rpc', // style
        'encoded', // use
        'Permite listar todos los proyectos');                   // description

$server->register('lista_provincia',
        array(), // parameter
        array('return' => 'tns:lista_provincia'), // output  array('return' => 'xsd:string'),     // output
        'urn:server', // namespace
        'urn:server#helloServer', // soapaction
        'rpc', // style
        'encoded', // use
        'Permite listar todas las provincias');                   // description

$server->register('lista_canton',
        array("provincia" => "xsd:string"), // parameter
        array('return' => 'tns:lista_canton'), // output  array('return' => 'xsd:string'),     // output
        'urn:server', // namespace
        'urn:server#helloServer', // soapaction
        'rpc', // style
        'encoded', // use
        'Permite listar todos los cantones de una provincia');                   // description

$server->register('obtener_departamento',
        array("id" => "xsd:int", "token" => "xsd:string"), // parameter
        array('return' => 'tns:departamento'), // output  array('return' => 'xsd:string'),     // output
        'urn:server', // namespace
        'urn:server#obtener_departamento', // soapaction
        'rpc', // style
        'encoded', // use
        'Permite obtener un departamento');                   // description
//----------------- FIN DE REGUSTROS -------//

function lista_departamento($token) {
    $wsdl = new \contoller\Servicio();
    if($token && $token == 'EXAMEN_4_B') {
        $datos = $wsdl->obtener_departamentos();
        return $datos;
    } else {
        return new soap_fault('500', '', 'Falta token', '');
    }
}
function lista_proyecto($token) {
    $wsdl = new \contoller\Servicio();
    if($token && $token == 'EXAMEN_4_B') {
        $datos = $wsdl->obtener_proyectos();
        return $datos;
    } else {
        return new soap_fault('500', '', 'Falta token', '');
    }
}
function lista_provincia() {
    $wsdl = new \contoller\Servicio();
    $datos = $wsdl->obtener_provincias_all();
    return $datos;
}
function lista_canton($provincia) {
    $wsdl = new \contoller\Servicio();
    $datos = $wsdl->obtener_cantones($provincia);
    return $datos;
}

function obtener_departamento($id, $token) {
    $wsdl = new \contoller\Servicio();
    if($token && $token == 'EXAMEN_4_B') {
        $datos = $wsdl->getDepartamento($id);
    return $datos;
    } else {
        return new soap_fault('500', '', 'Falta token', '');
    }
}

$server->service(file_get_contents("php://input"));