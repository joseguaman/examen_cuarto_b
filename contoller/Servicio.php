<?php

namespace contoller;

require_once 'conexion.php';

class Servicio {

    protected $conexion;

    public function __construct() {
        $this->conexion = new Conexion();
    }

    private function leerDataPaises() {
        $data = file_get_contents("./datos/country.json");
        return $data;
    }

    private function leerDataProvincia() {
        $data = file_get_contents("./datos/provincias.txt");
        return $data;
    }

    private function convertirArreglo($json) {

        $products = json_decode($json, true);
        return $products;
    }

    function cargarTodosPaises() {
        $paises = $this->leerDataPaises();
        echo $paises;
    }

    function buscar_pais_codigo($codigo) {
        $paises = $this->leerDataPaises();
        $products = $this->convertirArreglo($paises);
        $pais = "";
        foreach ($products as $product) {
            if ($product["alpha2Code"] == $codigo) {
                //print_r($product);
                $pais = $product;
                break;
            }
        }
        echo json_encode($pais);
    }

    function buscar_pais_espanol() {
        $paises = $this->leerDataPaises();
        $products = $this->convertirArreglo($paises);
        $pais = array();
        foreach ($products as $product) {
            $lenguaje = $product["languages"];
            $name = $lenguaje[0];
            //echo $name["iso639_1"].'..'.strpos($name["iso639_1"], "es").'---';
            //if(strpos($name["iso639_1"], "es") != false) {
            //echo $name["iso639_1"].'</br>';
            if ($name["iso639_1"] == "es") {
                //print_r($product);
                $pais[] = $product;
                //  break;
            }
        }
        echo json_encode($pais);
    }

    function obtener_provincias_all() {
        $file = fopen("./datos/provincias.txt", "r") or exit("Unable to open file!");
        $provincia = array();
        while (!feof($file)) {
            $data = fgets($file);
            $item = explode(".-.", $data);
            if (count($item) == 2) {
                $provincia[] = array("nombre" => $item[0], "region" => trim($item[1]));
            }
        }
        fclose($file);
        //echo json_encode($provincia);
        return $provincia;
    }

    function obtener_cantones($provinvia) {
        $file = fopen("./datos/cantones.txt", "r") or exit("Unable to open file!");
        $cantones1 = array();
        while (!feof($file)) {
            $data = fgets($file);
            $data = utf8_decode($data);
            $item = explode(",", $data);
            if ($provinvia == $item[0]) {
                $cantones1[] = array("canton" => trim($item[1]), "provincia" => $item[0]);
            }
        }
        fclose($file);
        //    echo json_encode($cantones1);
        return $cantones1;
    }

    function obtener_departamentos() {
        $obj = $this->conexion->conexion();
        $query = "select * from departamento";
        $stmt = $obj->prepare($query);
        $stmt->execute();
        $datos = array();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $datos[] = $row;
        }
        //echo json_encode($datos);
        return $datos;
    }

    private function gen_uuid() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                // 32 bits for "time_low"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff),
                // 16 bits for "time_mid"
                mt_rand(0, 0xffff),
                // 16 bits for "time_hi_and_version",
                // four most significant bits holds version number 4
                mt_rand(0, 0x0fff) | 0x4000,
                // 16 bits, 8 bits for "clk_seq_hi_res",
                // 8 bits for "clk_seq_low",
                // two most significant bits holds zero and one for variant DCE1.1
                mt_rand(0, 0x3fff) | 0x8000,
                // 48 bits for "node"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    function getDepartamento($external) {
        $obj = $this->conexion->conexion();
        $query = "select * from departamento where id = :id";
        $stmt = $obj->prepare($query);
        $stmt->bindParam("id", $external, \PDO::PARAM_INT);
        $stmt->execute();
        $datos = array();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $data = array(
                "nombre" => $row["nombre"],
                "tipo" => $row["tipo"]);
            $datos = $data;
        }
        //echo json_encode($datos);
        return $datos;
    }

    function obtener_proyectos() {
        $obj = $this->conexion->conexion();
        $query = "select * from proyecto";
        $stmt = $obj->prepare($query);
        $stmt->execute();
        $datos = array();
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $item = array(
                "external" => $row["external_id"],
                "nombre" => $row["nombre"],
                "f_inicio" => $row["fecha_inicio"],
                "f_finalizacion" => $row["fecha_finalizacion"],
                "provincia" => $row["provincia"],
                "canton" => $row["canton"],
                "pais" => $row["pais"],
                "extranjero" => $row["extranjero"],
                "descripcion" => $row["descripcion"],
                "monto" => $row["monto"],
                "id_departamento" => $row["id_departamento"]
            );
            $datos[] = $item;
        }
        //echo json_encode($datos);
        return $datos;
    }

    function guardar($token, $data) {
        if ($token != "" && $token != "EXAMEN_4_B") {
            return array("message" => "No puede acceder a este recurso",
                "codigo" => "401");
        } else {
            $obj = $this->conexion->conexion();
            $external = $this->gen_uuid();
            $query = "INSERT INTO proyecto (external_id, nombre, fecha_inicio, "
                    . "fecha_finalizacion, tiempo, provincia, canton, "
                    . "pais, extranjero, descripcion, monto, id_departamento) "
                    . "values(:external, :nombre, :fi, :ff, :time, :prov, :cant, :pais, :extran, "
                    . ":desc, :monto, :id)";
            $stmt = $obj->prepare($query);
            $stmt->bindParam("external", $external, \PDO::PARAM_STR);
            $stmt->bindParam("nombre", $data['nombre'], \PDO::PARAM_STR);
            $stmt->bindParam("fi", $data['fi'], \PDO::PARAM_STR);
            $stmt->bindParam("ff", $data['ff'], \PDO::PARAM_STR);
            $stmt->bindParam("time", $data['time'], \PDO::PARAM_INT);
            $stmt->bindParam("prov", $data['prov'], \PDO::PARAM_STR);
            $stmt->bindParam("cant", $data['cant'], \PDO::PARAM_STR);
            $stmt->bindParam("pais", $data['pais'], \PDO::PARAM_STR);
            $stmt->bindParam("extran", $data['extran'], \PDO::PARAM_BOOL);
            $stmt->bindParam("desc", $data['desc'], \PDO::PARAM_STR);
            $stmt->bindParam("monto", $data['monto'], \PDO::PARAM_INT);
            $stmt->bindParam("id", $data['id'], \PDO::PARAM_INT);
            $stmt->execute();
            $newId = $obj->lastInsertId();
            if ($newId > 0) {
                return array("mensaje" => "Se ha guardado correctamente",
                    "codigo" => "200"
                );
            } else {
                return array();
            }
        }
    }
    
    function modificar($token, $data) {
        if ($token != "" && $token != "EXAMEN_4_B") {
            return array("message" => "No puede acceder a este recurso",
                "codigo" => "401");
        } else {
            $obj = $this->conexion->conexion();
            
            $query = "UPDATE proyecto set nombre = :nombre, fecha_inicio = :fi, "
                    . "fecha_finalizacion = :ff, tiempo = :time, "
                    . "provincia = :prov, canton = :cant, "
                    . "pais = :pais, extranjero = :extran, descripcion = :desc, "
                    . "monto = :monto, id_departamento = :id "
                    . " where external_id = :external";
            $stmt = $obj->prepare($query);
            $stmt->bindParam("external", $data['external'], \PDO::PARAM_STR);
            $stmt->bindParam("nombre", $data['nombre'], \PDO::PARAM_STR);
            $stmt->bindParam("fi", $data['fi'], \PDO::PARAM_STR);
            $stmt->bindParam("ff", $data['ff'], \PDO::PARAM_STR);
            $stmt->bindParam("time", $data['time'], \PDO::PARAM_INT);
            $stmt->bindParam("prov", $data['prov'], \PDO::PARAM_STR);
            $stmt->bindParam("cant", $data['cant'], \PDO::PARAM_STR);
            $stmt->bindParam("pais", $data['pais'], \PDO::PARAM_STR);
            $stmt->bindParam("extran", $data['extran'], \PDO::PARAM_BOOL);
            $stmt->bindParam("desc", $data['desc'], \PDO::PARAM_STR);
            $stmt->bindParam("monto", $data['monto'], \PDO::PARAM_INT);
            $stmt->bindParam("id", $data['id'], \PDO::PARAM_INT);
            
            if ($stmt->execute()) {
                return array("mensaje" => "Se ha modificado",
                    "codigo" => "200"
                );
            } else {
                return array();
            }
        }
    }

}
